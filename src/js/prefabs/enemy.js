import Bullet from './bullet';

export default class Enemy extends Phaser.Sprite {

    constructor({ game, x, y, asset, frame, health, bulletSpeed }) {
        super(game, x, y, asset, frame);

        this.game = game;

        this.anchor.setTo(0.5);
        this.scale.setTo(0.8);
        this.health = health;
        this.maxHealth = health;
        this.game.physics.arcade.enable(this);

        this.animations.add('spinning', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 30, true);
        this.play('spinning');

        var game  = this.game;
        var sprite = this; // game.add.sprite(0, 0, 'alien');
        var text = game.add.text(10, 10, "Health", {font: "16px Arial", fill: "#ffffff"});
        sprite.addChild(text);
        this.text = text;
    }
    
    update() {
    	this.text.setText("Health: " + this.health);
       
    }


    reset({ x, y, health, bulletSpeed, speed }) {
        super.reset(x, y, health);
    }
}
