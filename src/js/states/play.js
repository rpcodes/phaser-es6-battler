import Player from '../prefabs/player';
import Enemy from '../prefabs/enemy';
import HUD from '../prefabs/hud';

export default class Play extends Phaser.State {

    create() {

        this.farback = this.add.tileSprite(0, 0, 800, 2380, 'farback');

        this.game.time.slowMotion = 1;

        this.enemies = this.add.group();
        this.enemies.enableBody = true;

        let enemy = this.enemies.getFirstExists(false);
        let data = {
                game: this.game,
                x: this.game.rnd.integerInRange(6, 76) * 10,
                y: 50,
                speed: {
                    x: 0,
                    y: 0
                },
                health: 9,
                bulletSpeed: 0,
                asset: 'alien'
            };
        if (!enemy) {
            enemy = new Enemy(data);
            this.enemies.add(enemy);
        }
        enemy.reset(data);


        this.game.input.onDown.add(() => {
            this.game.time.slowMotion = 1;
        });

        this.game.input.onUp.add(() => {
            this.game.time.slowMotion = 1;
        });

        this.enemyTime = 0;
        this.enemyInterval = 1.5;
        this.enemyShootTime = 0;
        this.enemyShootInterval = 1;
        this.playerShootTime = 0;
        this.playerShootInterval = 0.16;

        this.game.time.events.loop(Phaser.Timer.SECOND * 10, () => {
            if(this.enemyInterval > 0.2 ){
                this.enemyInterval -= 0.1;
            }
        });

        this.overlayBitmap = this.add.bitmapData(this.game.width, this.game.height);
        this.overlayBitmap.ctx.fillStyle = '#000';
        this.overlayBitmap.ctx.fillRect(0, 0, this.game.width, this.game.height);

        this.overlay = this.add.sprite(0, 0, this.overlayBitmap);
        this.overlay.visible = false;
        this.overlay.alpha = 0.75;

        this.music = this.game.add.audio('playMusic');
        this.bulletHitSound = this.add.sound('bulletHit');
        this.enemyExplosionSound = this.add.sound('enemyExplosion');
        this.playerExplosionSound = this.add.sound('playerExplosion');
        this.gameOverSound = this.add.sound('gameOver');

        this.music.loopFull();

        this.initWeaponHitSprite();
        // add events to check for swipe
        this.game.input.onDown.add(this.onDown, this);
        this.game.input.onUp.add(this.onUp, this);
        this.game.input.addMoveCallback(this.onMove, this);
    
    }
    
    initWeaponHitSprite(){
        this.weaponSprite = this.game.add.sprite(10,10, 'bullet');
        this.game.physics.arcade.enable(this.weaponSprite);
    	this.weaponSprite.body.enable = false;
    	this.weaponSprite.visible = false;
    }
    
    onMove(pointer, x, y, click) {
    	if (pointer.id != 0){
    		return;
    	}

    	this.weaponSprite.visible = true;
    	this.isTap = click; // change to player.lightAttack
    	if (this.weaponSprite != null){
    		this.weaponSprite.x = pointer.x;
    		this.weaponSprite.y = pointer.y;
    	}
    	
    }
    onDown(pointer) {
    	this.start_swipe_point = new Phaser.Point(pointer.x, pointer.y);
    	this.end_swipe_point = null;
    	this.weaponSprite.x = pointer.x;
    	this.weaponSprite.y = pointer.y;

        this.weaponSprite.body.enable = true;
        console.log("ENABLED");
    };
    onUp(pointer){
    	console.log("onUp start")
    	this.end_swipe_point = new Phaser.Point(pointer.x, pointer.y);
    	var swipe_length = Phaser.Point.distance(this.end_swipe_point, this.start_swipe_point);
	    // if the swipe length is greater than the minimum, a swipe is detected
//	    if (swipe_length >= this.MINIMUM_SWIPE_LENGTH) {
//	        // create a new line as the swipe and check for collisions
//	        cut_style = {line_width: 5, color: 0xE82C0C, alpha: 1}
//	        cut = new FruitNinja.Cut(this, "cut", {x: 0, y: 0}, {group: "cuts", start: this.start_swipe_point, end: this.end_swipe_point, duration: 0.3, style: cut_style});
//	        this.swipe = new Phaser.Line(this.start_swipe_point.x, this.start_swipe_point.y, this.end_swipe_point.x, this.end_swipe_point.y);
//	    }

    	this.swipe = new Phaser.Line(this.start_swipe_point.x, 
    			this.start_swipe_point.y, 
    			this.end_swipe_point.x, this.end_swipe_point.y);


    	this.weaponSprite.body.enable = false;
    	this.weaponSprite.visible = false;
        
    }
    
    update() {
    		this.game.physics.arcade.overlap(this.weaponSprite, this.enemies, 
    				(weapon, enemy) => {
    			console.log("OK " + enemy);
    			this.isTap
    			enemy.health -= this.isTap ? 1 : 5;
    			this.hitEffect(enemy, "#FF0");
    			this.weaponSprite.body.enable = false;
    		})
    }

    createEnemy(data) {

        let enemy = this.enemies.getFirstExists(false);

        if (!enemy) {
            enemy = new Enemy(data);
            this.enemies.add(enemy);
        }
        enemy.reset(data);
    }

    hitEffect(obj, color) {
        let tween = this.game.add.tween(obj);
        let emitter = this.game.add.emitter();
        let emitterPhysicsTime = 0;
        let particleSpeed = 100;
        let maxParticles = 10;

        tween.to({tint: 0xff0000}, 100);
        tween.onComplete.add(() => {
            obj.tint = 0xffffff;
        });
        tween.start();

        emitter.x = obj.x;
        emitter.y = obj.y;
        emitter.gravity = 0;
        emitter.makeParticles('particle');

        if (obj.health <= 0) {
            particleSpeed = 200;
            maxParticles = 40;
            color = 0xff0000;
        }

        emitter.minParticleSpeed.setTo(-particleSpeed, -particleSpeed);
        emitter.maxParticleSpeed.setTo(particleSpeed, particleSpeed);
        emitter.start(true, 500, null, maxParticles);
        emitter.update = () => {
            emitterPhysicsTime += this.game.time.physicsElapsed;
            if(emitterPhysicsTime >= 0.6){
                emitterPhysicsTime = 0;
                emitter.destroy();
            }

        };
        emitter.forEach(particle => particle.tint = color);
        
    }


    gameOver(){
        this.game.time.slowMotion = 3;
        this.overlay.visible = true;
        this.game.world.bringToTop(this.overlay);
        let timer = this.game.time.create(this.game, true);
        timer.add(3000, () => {
            this.music.stop();
            this.gameOverSound.play();
            this.game.state.start('Menu');
        });
        timer.start();
    }

}
